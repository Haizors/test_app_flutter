void main(List<String> args) {
  List arr = [
    1,
    2,
    3,
    "đây",
    "kết",
    "là",
    true,
    false,
    {true: "buổi", 1: "học", 10.2: ":", false: "dart basics"},
    ['thứ', 'quả', 'về'],
    "(phần 1)",
    {"flutter": "dart"},
  ];

  String s1 =
      "${arr[3].toString().substring(0, 1).toUpperCase()}${arr[3].toString().substring(1, 3)}";

  String s2 =
      "${arr[5]} ${arr[4]} ${arr[9][1]} ${arr[8][true]} ${arr[8][1]} ${arr[9][0]} ${arr[1]} ${arr[9][2]} ${arr.last["flutter"].toString().substring(0, 1).toUpperCase()}${arr.last["flutter"].toString().substring(1, 4)}${arr[8][10.2]}";

  String s3 = "${arr[8][false].toString().toUpperCase()} ${arr[10]}";
  print("$s1 $s2 $s3");
}
