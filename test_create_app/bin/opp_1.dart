void main() {
  Person person = Person('Hai', 20, 'trunghaihoang1205@gmail.com');
  person.showName();
  person.showAge();

  Student student =
      Student("Hai", 20, "trunghaihoang1205@gmail.com", 12, '12A', 8);
  student.showMarkPoint();

  Teacher teacher = Teacher('Hong', 30, 'hng19@gmail.com', 'Education', 'math');
  teacher.teach();
}

class Person {
  String name;
  int age;
  String? email;

  Person(this.name, this.age, this.email);

  void showName() {
    print('Name: $name');
  }

  void showAge() {
    print('Age: $age');
  }
}

class Student extends Person {
  int grade;
  String className;
  int mark;

  Student(String name, int age, String? email, this.grade, this.className,
      this.mark)
      : super(name, age, email);

  void showMarkPoint() {
    print('Mark point: $mark');
  }
}

class Teacher extends Person {
  String department;
  String subject;

  Teacher(String name, int age, String? email, this.department, this.subject)
      : super(name, age, email);

  void teach() {
    print('Teaching $subject in $department department');
  }
}
